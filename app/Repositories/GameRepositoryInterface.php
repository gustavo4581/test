<?php declare(strict_types=1);

namespace App\Repositories;

interface GameRepositoryInterface
{
    public function all();
}
