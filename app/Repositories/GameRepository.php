<?php declare(strict_types=1);

namespace App\Repositories;

use App\Models\Game;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\CommonMark\Util\ArrayCollection;

class EntityRepository implements GameRepositoryInterface
{
    protected Game $model;

    /**
     * EntityRepository constructor.
     *
     * @param Game $game
     */
    public function __construct(Game $game)
    {
        $this->model = $game;
    }

    public function has(string $name)
    {
        // TODO: Implement has() method.
    }

    public function get(string $name)
    {
        // TODO: Implement get() method.
    }

    public function set(string $name, string $value)
    {
        // TODO: Implement set() method.
    }

    public function clear(string $name)
    {
        // TODO: Implement clear() method.
    }

    public function all(): Collection
    {
        return $this->model->all();
    }
}
