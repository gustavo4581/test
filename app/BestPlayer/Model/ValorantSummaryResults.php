<?php declare(strict_types = 1);

namespace App\BestPlayer\Model;

class ValorantSummaryResults
{

    private string $playerName;
    private string $teamName;
    private int $kills;
    private int $deaths;


    private function __construct(
        string $playerName,
        string $teamName,
        int $kills,
        int $deaths
    ) {
        $this->playerName = $playerName;
        $this->teamName = $teamName;
        $this->kills = $kills;
        $this->deaths = $deaths;
    }

   public function getPlayerName(): string
   {
       return $this->playerName;
   }
   public function getTeamName(): string
   {
       return $this->teamName;
   }
   public function getKills(): int
   {
       return $this->kills;
   }
   public function getDeaths(): int
   {
       return $this->deaths;
   }
}
