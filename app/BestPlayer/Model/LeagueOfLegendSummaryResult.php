<?php declare(strict_types = 1);

namespace App\BestPlayer\Model;

class LeagueOfLegendSummaryResult
{
    private string $playerName;
    private string $nickname;
    private string $teamName;
    private bool $winner;
    private string $position;
    private int $kills;
    private int $deaths;
    private int $assists;
    private int $damageDeal;
    private int $healDeal;

    private function __construct(
        string $playerName,
        string $nickname,
        string $teamName,
        bool $winner,
        string $position,
        int $kills,
        int $deaths,
        int $assists,
        int $damageDeal,
        int $healDeal
    ) {
        $this->playerName = $playerName;
        $this->nickname = $nickname;
        $this->teamName = $teamName;
        $this->winner = $winner;
        $this->position = $position;
        $this->kills = $kills;
        $this->deaths = $deaths;
        $this->assists = $assists;
        $this->damageDeal = $damageDeal;
        $this->healDeal = $healDeal;
    }

    public function getPlayerName(): string
    {
        return $this->playerName;
    }
    public function getNickname(): string
    {
        return $this->nickname;
    }
    public function getTeamName(): string
    {
        return $this->teamName;
    }
    public function isWinner(): bool
    {
        return $this->winner;
    }
    public function getPosition(): string
    {
        return $this->position;
    }
    public function getKills(): int
    {
        return $this->kills;
    }
    public function getDeaths(): int
    {
        return $this->deaths;
    }
    public function getAssists(): int
    {
        return $this->assists;
    }
    public function getDamageDeal(): int
    {
        return $this->damageDeal;
    }
    public function getHealDeal(): int
    {
        return $this->healDeal;
    }
}
