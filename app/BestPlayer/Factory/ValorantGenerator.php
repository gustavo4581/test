<?php declare(strict_types=1);

namespace App\BestPlayer\Factory;

use App\BestPlayer\Model\ValorantSummaryResults;
use App\Models\Game;
use App\Repositories\GameRepositoryInterface;

class ValorantGenerator implements ValorantFactoryInterface
{
    private GameRepositoryInterface $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function create(ValorantSummaryResults $summaryResult): array
    {
        $gameSummaries = $this->gameRepository->all();
        foreach ($gameSummaries as $gameSummary) {
            if ($gameSummary->game->name !== Game::LEAGUE_OF_LEGENDS) {
                continue;
            }

        }
    }

    private function getKDR(ValorantSummaryResults $summaryResult): int
    {
        return $summaryResult->getKills() / $summaryResult->getDeaths();
    }

    private function getWiningTeam(): PLayer
    {
        //TODO: Implement it wold be return a wining team
    }
}
