<?php declare(strict_types=1);

namespace App\BestPlayer\Factory;

use App\BestPlayer\Model\LeagueOfLegendSummaryResult;
use App\Models\Game;
use App\Repositories\GameRepositoryInterface;

class LeagueOfLegendsGenerator implements LeagueOfLegendFactoryInterface
{
    private GameRepositoryInterface $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function create(LeagueOfLegendSummaryResult $summaryResult): array
    {
        $gameSummaries = $this->gameRepository->all();

        foreach ($gameSummaries as $gameSummary) {
            if ($gameSummary->game->name !== Game::LEAGUE_OF_LEGENDS) {
                continue;
            }

        }

    }

    private function getKDA(LeagueOfLegendSummaryResult $summaryResult): int
    {
        return ($summaryResult->getKills() + $summaryResult->getAssists()) / $summaryResult->getDeaths();
    }
}
