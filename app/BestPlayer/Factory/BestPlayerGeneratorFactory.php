<?php declare(strict_types=1);

namespace App\BestPlayer\Factory;

use App\Models\Player;
use App\Repositories\GameRepositoryInterface;
use App\Models\Game;

class BestPlayerGeneratorFactory
{

    private GameRepositoryInterface $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository, PlayerRepositoryInterface $playerRepository)
    {
        $this->gameRepository = $gameRepository;
        $this->playerRepository = $playerRepository;
    }

    public function get(): ?Player
    {
        $players = $this->playerRepository->all();

        foreach ($players as  $player) {
            $games = $this->gameRepository->findByPlayer($player);
            $this->getRatingPoints($player, $games);
        }

        
    }

    private function getExtraPoints(Game $game): int
    {
        $extraPoints = 0;

        if ($game->getWinner() === $player) {
            $extraPoints += 10;
        }

        if ($game->getWinner() === $player) {
            $extraPoints += 10;
        }

        return $extraPoints;
    }

    private function getRatingPoints($player, $games)
    {
        $ratingPoints = 0;

        foreach ($games as $game) {
            switch ($game->getType()) {
                case Game::LEAGUE_OF_LEGENDS:
                    $ratingPoints += new LeagueOfLegendsGenerator($game);
                    break;
                case Game::VALORANT:
                    $ratingPoints += new ValorantGenerator($game);
                    break;
                default:
                    $ratingPoints += 0;
                    break;
            }
        }

        $player->setRatingPoints($ratingPoints);
    }
}
