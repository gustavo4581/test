<?php declare(strict_types=1);

namespace App\BestPlayer\Factory;

use App\BestPlayer\Model\ValorantSummaryResults;
use App\Models\Player;

interface BestPlayerFactoryGeneratorInterface
{
    public function generate(string $gameType): Player;
}
