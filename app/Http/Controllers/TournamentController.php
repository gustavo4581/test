<?php declare(strict_types = 1);

namespace App\Http\Controllers;

use App\BestPlayer\Factory\BestPlayerGeneratorFactory;
use App\BestPlayer\Factory\BestPlayerFactoryGeneratorInterface;
use App\Models\Player;
use App\Repositories\GameRepositoryInterface;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class TournamentController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    private BestPlayerFactoryGeneratorInterface $bestPlayerFactory;
    private GameRepositoryInterface $gameRepository;

    public function __construct(BestPlayerFactoryGeneratorInterface $bestPlayerFactory, GameRepositoryInterface $gameRepository)
    {
        $this->bestPlayerFactory = $bestPlayerFactory;
        $this->gameRepository = $gameRepository;
        $this->playerRepository = $playerRepository;
    }

    public function getBestPlayer(): Player
    {
        $bestPlayer= new BestPlayerGeneratorFactory($this->playerRepository);

        return $bestPlayer->get();
    }

}

